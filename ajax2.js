var listCategories = [];
var aforoMin;
var categoriaId;
var numeroHabitaciones;
var categoriaName;
var categoriaPrice;
var noDisponible;
var dataResFin;
var dataRes;
var dni;
var idCliente;
var cliente;
var habitacion;
var noDisponibleHabitacion = [];
var ip="http://51.210.15.112:8181/";

function encontrarHabitacion() {
  var habitacionEncontrada = false;
  //console.log(noDisponibleHabitacion[0].id)
  if (!isEmpty(noDisponibleHabitacion)) {
    
    for (var i = 0; i < noDisponibleHabitacion.length; i++) {
      if (habitacionEncontrada == false) {
        for (var o = 0; o < noDisponible.length; o++) {
          //console.log("habitacio no trobada: "+noDisponible[o].habitacion.id);
          if (noDisponibleHabitacion[i].id != noDisponible[o].habitacion.id) {
            habitacionEncontrada = true;
            habitacion = noDisponible[o].habitacion;
            //console.log("habitacio trobada: "+noDisponible[o].habitacion.id);
          }
        }
      }
    }
    if(!habitacion) {
      //console.log("passa per aqui");
      listarHabiracionesBug();
    }
  } else {
    encontrarPrimeraReserva();
  }
  registrarReserva();
}

function isEmpty(val){
  return (val === undefined || val == null || val.length <= 0) ? true : false;
}

function listarHabiracionesBug() {
  aforoMin = $("#aforo").val();
    //console.log("debug");
    $.ajax({
      async: false,
      url: ip+"api/habitaciones",
      method: "GET",
      dataType: "json",
      headers: {
        Accept: "application/json",
      },
      success: function (data) {
     var sortir=false;
        for(var i=0; i<data.length;i++) {
          if(sortir==false) {
          if(data[i].categoria.id==categoriaId) {
           // console.log(categoriaId+" "+data[i].categoria.id);
            for (var o = 0; o < noDisponibleHabitacion.length; o++) {
            if(data[i].id!=noDisponibleHabitacion[o].id) {
              habitacion=data[i];
              sortir=true;
          }
        }
          }
        }
        }
      },
  
      error: function (error) {
        alert(error);
      },
    });
}
function encontrarPrimeraReserva() {
    aforoMin = $("#aforo").val();
    //console.log("debug");
    $.ajax({
      async: false,
      url: ip+"api/habitaciones",
      method: "GET",
      dataType: "json",
      headers: {
        Accept: "application/json",
      },
      success: function (data) {
     var sortir=false;
        for(var i=0; i<data.length;i++) {
          if(sortir==false) {
          if(data[i].categoria.id==categoriaId) {
            habitacion=data[i];
            sortir=true;
          }
        }
        }
      },
  
      error: function (error) {
        alert(error);
      },
    });
}

function registrarReserva() {
  $.ajax({
    async: false,
    url: ip+"api/reservas",
    method: "POST",

    dataType: "json",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    //contentType: 'application/x-www-form-urlencoded',
    data: JSON.stringify({
      estado: 0,
      fecha_inicio: dataRes,
      fecha_final: dataResFin,
      cliente: cliente,
      habitacion: habitacion,
    }),

    success: function (data, textStatus, jqXHR) {
      alert("Reserva echa en habitacion: " + habitacion.puerta);
      window.location.replace("http://51.210.15.112:8100/inicio/"); 
    },

    error: function (error) {
      alert(error + "error");
    },
  });
}

function registrarCliente() {
  var nombreCliente = $("#nombreCliente").val();
  var telefonoCliente = $("#telefonoCliente").val();
  var emailCliente = $("#emailCliente").val();
  if (
    nombreCliente != "" &&
    telefonoCliente >= 100000000 &&
    telefonoCliente <= 999999999 &&
    emailCliente.includes("@")
  ) {
    postCliente(nombreCliente, telefonoCliente, emailCliente);
    //alert(cliente.dni + cliente.nombre);
  } else {
    alert("Alguno de los campos introducidos no es valido");
  }
}

function postCliente(nombreCliente, telefonoCliente, emailCliente) {
  $.ajax({
    async: false,
    url: ip+"api/clientes",
    method: "POST",

    dataType: "json",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    //contentType: 'application/x-www-form-urlencoded',
    data: JSON.stringify({
      dni: dni,
      nombre: nombreCliente,
      telefono: telefonoCliente,
      email: emailCliente,
    }),

    success: function (data, textStatus, jqXHR) {
      canviarBarra(100);
      cliente = data;
      $("#divCrearCliente").hide();
      $("#barra2").show();
      $("#CategoriaNombre").text("Tu nombre: " + cliente.nombre);
      $("#CategoriaDni").text("Tu DNI: " + cliente.dni);
      $("#CategoriaEmail").text("Email: " + cliente.email);
      $("#CategoriaTelefono").text("Telefono: " + cliente.telefono);
      $("#divConfirmar").show();
    },

    error: function (error) {
      alert(error + "error");
    },
  });
}
function canviarBarra(progres) {
  $("#barra")
    .attr("aria-valuenow", progres)
    .css("width", progres + "%");
}

function comprovarDNI() {
  dni = $("#dni").val();
  if (dni.length != 9) {
    alert("Este campo tiene que tener 9 caracteres");
  } else {
    idCliente = estaDNI();
    if (idCliente == -1) {
      $("#divComprovarDNI").hide();
      $("#divCrearCliente").show();
      $("#DNICrear2").text("Tu DNI: " + dni);
    } else {
      $("#divComprovarDNI").hide();
      $("#CategoriaNombre").text("Tu nombre: " + cliente.nombre);
      $("#CategoriaDni").text("Tu DNI: " + cliente.dni);
      $("#CategoriaEmail").text("Email: " + cliente.email);
      $("#CategoriaTelefono").text("Telefono: " + cliente.telefono);
      canviarBarra(100);
      $("#barra2").show();
      $("#divConfirmar").show();
    }
  }
}

function estaDNI() {
  var id = -1;
  $.ajax({
    async: false,
    url: ip+"api/clientes/",
    method: "GET",
    dataType: "json",
    headers: {
      Accept: "application/json",
    },
    success: function (data) {
      console.log("arriba");
      for (var i = 0; i < data.length; i++) {
        if (data[i].dni == dni) {
          id = data[i].id;
          cliente = data[i];
        }
      }
    },

    error: function (error) {
      alert(error);
    },
  });
  return id;
}

function omplirLlista() {
  for (var i = 0; i < listCategories.length; i++) {
    $("#selectList").append(
      new Option(
        listCategories[i].nombre + "--------" + listCategories[i].precio + "€",
        listCategories[i].id
      )
    );
  }
}
function inici() {
  $("#divCategoria").hide();
  $("#divFechaFin").hide();
  $("#barra2").hide();
  $("#divComprovarDNI").hide();
  var today = new Date();
  $("#barra").addClass("progress-bar-ocre");
  $("#divCrearCliente").hide();
  $("#divConfirmar").hide();

  document.getElementById("dia").value =
    today.getFullYear() +
    "-" +
    ("0" + (today.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + today.getDate()).slice(-2);
  document.getElementById("dia").min =
    today.getFullYear() +
    "-" +
    ("0" + (today.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + today.getDate()).slice(-2);
}

function comprovarFechaFin() {
  if ($("#diaFin").val() == $("#dia").val()) {
    alert("No puedes hacer una reserva que termine ese mismo dia");
  } else {
    var numConcidencies = 0;
    dataResFin = $("#diaFin").val();
    for (var i = 0; i < noDisponible.length; i++) {
      if (dataResFin == noDisponible[i].diaReserva) {
        //console.log(noDisponible[i].diaReserva);
        noDisponibleHabitacion.push(noDisponible[i].habitacion);
        numConcidencies++;
      }
    }
    if (numConcidencies >= numeroHabitaciones) {
      alert("No quedan habitaciones de este tipo para este dia");
    } else {
      $("#CategoriaDiaFin").text("Dia salida: " + dataResFin);
      $("#divFechaFin").hide();
      $("#divComprovarDNI").show();
      canviarBarra(75);
    }
  }
}

function comprovarRes() {
  if ($("#dia").val() == "") {
    alert("Introduce un dia, por favor");
  } else {
    //console.log($("#selectList").val());
    numHabitaciones($("#selectList").val());
    buscarPreuCategoria($("#selectList").val());
    categoriaId = $("#selectList").val();
    noDisponible = llistaNegra($("#selectList").val());
    dataRes = $("#dia").val();
    //console.log(noDisponible[0]);
    //console.log(dataRes);
    var numConcidencies = 0;
    for (var i = 0; i < noDisponible.length; i++) {
      //console.log(noDisponible[i].diaReserva);
      if (dataRes == noDisponible[i].diaReserva) {
        noDisponibleHabitacion.push(noDisponible[i].habitacion);
        numConcidencies++;
      }
    }
    if (numConcidencies >= numeroHabitaciones) {
      alert("No quedan habitaciones de este tipo para este dia");
    } else {
      $("#divCategoria").hide();
      document.getElementById("diaFin").value = $("#dia").val();
      document.getElementById("diaFin").min = $("#dia").val();
      $("#divFechaFin").show();
      $("#CategoriaNom").text("Categoria: " + categoriaName);
      $("#CategoriaPreu").text("Precio por dia: " + categoriaPrice + "€");
      $("#CategoriaDia").text("Dia entrada: " + dataRes);
      canviarBarra(50);
    }
  }
}

function buscarPreuCategoria(id) {
  aforoMin = $("#aforo").val();
  $.ajax({
    async: false,
    url: ip+"api/categorias/" + id,
    method: "GET",
    dataType: "json",
    headers: {
      Accept: "application/json",
    },
    success: function (data) {
      categoriaPrice = data.precio;
      // console.log(data.precio);
      categoriaName = data.nombre;
      // console.log(data.nombre);
    },

    error: function (error) {
      alert(error);
    },
  });
}

function buscarCategories() {
  aforoMin = $("#aforo").val();
  $.ajax({
    async: false,
    url: ip+"api/habitaciones",
    method: "GET",
    dataType: "json",
    headers: {
      Accept: "application/json",
    },
    success: function (data) {
      for (var i = 0; i < data.length; i++) {
        //console.log(data[i].categoria.aforo);
        if (data[i].categoria.aforo >= aforoMin) {
          var bool = true;
          for (var o = 0; o < listCategories.length; o++) {
            //console.log(data[i].categoria.id+" != "+list[o]);
            if (data[i].categoria.id == listCategories[o].id) {
              bool = false;
            }
          }
          if (bool == true) {
            listCategories.push(data[i].categoria);
          }
        }
      }
    },

    error: function (error) {
      alert(error);
    },
  });

  /*for(var o=0;o<listCategories.length;o++) {
            console.log(listCategories[o].nombre);
        }*/
  if (listCategories.length != 0) {
    $("#divAforo").hide();
    $("#divCategoria").show();
    $("#Aforo").text("Numero de personas: " + aforoMin);
    // $("#barra").aria-valuenow("25");
    canviarBarra(25);

    omplirLlista();
  } else {
    alert("No existen habitaciones con este aforo");
  }
}

function numHabitaciones(numCategoria) {
  $.ajax({
    async: false,
    url: ip+"api/habitaciones/num/" + numCategoria,
    method: "GET",
    dataType: "json",
    headers: {
      Accept: "application/json",
    },
    success: function (data) {
      numeroHabitaciones = data;
    },

    error: function (error) {
      alert(error);
    },
  });
}

function llistaNegra(numCategoria) {
  var nopot = [];
  //console.log(numCategoria);
  $.ajax({
    async: false,
    url: ip+"api/reservas/ocupados/" + numCategoria,
    method: "GET",
    dataType: "json",
    headers: {
      Accept: "application/json",
    },
    success: function (data) {
      nopot = data;
      // console.log(data[0]);
      //console.log(nopot[0].diaReserva);
    },

    error: function (error) {
      alert(error);
    },
  });
  return nopot;
}
